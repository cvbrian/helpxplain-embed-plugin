<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 *
 * Plugin Name:       HelpXplain Embed
 * Plugin URI:        https://www.planetnine.tech
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Brian Penner
 * Author URI:        https://www.planetnine.tech
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       helpxplain-embed
 * Domain Path:       /languages
 */

function register_helpxplain() {
    wp_register_script( 'helpxplain-xploader', plugins_url( 'xploader.js', __FILE__ ), null, null, true );
}
add_action( 'wp_enqueue_scripts', 'register_helpxplain' );

function helpxplain_shortcode( $atts) {

    if (is_array($atts) && array_key_exists("data-url", $atts)) {

        
        $safe_opts = array('data-url', 'data-aspect', 'data-title', 'data-caption', 'data-lightbox', 'data-lightbox', 'data-load', 'data-playbutton', 'data-startat', 'data-playafter', 'data-debug');
        
        $opts = array_intersect_key($atts, array_flip($safe_opts));
        
        $html = "<div class='helpxplain' ";
        
        foreach ($opts as $key => $val) {
            $html .= $key . "='" . $val . "'";
        };
        
        $html .= "></div>";
    } else {
        $html = "<div>Please include data-url in your shortcode.</div> <div>See the available attribute <a href='https://www.helpandmanual.com/help/gf-developers_webpages.html'>HERE</a> under Advanced loader script embedding method
.</div>";
    };
    
    wp_enqueue_script( 'helpxplain-xploader' );
    
    return $html;
}
add_shortcode( 'helpxplain', 'helpxplain_shortcode' );